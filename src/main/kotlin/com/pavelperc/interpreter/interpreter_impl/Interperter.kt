package com.pavelperc.interpreter.interpreter_impl

import com.pavelperc.interpreter.ast.*


typealias VarScope = Map<String, Int>

open class InterpreterError(msg: String, val context: String, val lineNumber: Int) :
    IllegalStateException("$msg $context:$lineNumber")

class RuntimeError(expr: Expr) : InterpreterError("RUNTIME ERROR", expr.toString(), expr.lineNumber)
class StackOverflowError(callExpr: CallExpr) : InterpreterError("STACK OVERFLOW", callExpr.funName, callExpr.lineNumber)

class ParamNotFound(id: Identifier) : InterpreterError("PARAMETER NOT FOUND", id.name, id.lineNumber)

class FunctionNotFound(callExpr: CallExpr) :
    InterpreterError("FUNCTION NOT FOUND", callExpr.funName, callExpr.lineNumber)

class ArgNumberMismatch(callExpr: CallExpr) :
    InterpreterError("ARGUMENT NUMBER MISMATCH", callExpr.funName, callExpr.lineNumber)


class Interpreter(val program: Program) {
    
    // todo check name collisions??
    val funNameToDef = program.funDefs.map { it.funName to it }.toMap()
    
    fun Expr.execute(varScope: VarScope = emptyMap()): Int = when (this) {
        is BinOperator -> execute(varScope)
        is NumberExpr -> value
        is Identifier -> varScope[name] ?: throw ParamNotFound(this)
        is CallExpr -> execute(varScope)
        is IfExpr -> execute(varScope)
    }
    
    fun IfExpr.execute(varScope: VarScope = emptyMap()): Int =
        if (condition.execute(varScope) != 0) {
            onTrue.execute(varScope)
        } else {
            onFalse.execute(varScope)
        }
    
    fun CallExpr.execute(varScope: VarScope = emptyMap()): Int {
        val funDef = funNameToDef[funName] ?: throw FunctionNotFound(this)
        
        if (funDef.params.size != arguments.size) {
            throw ArgNumberMismatch(this)
        }
        
        val computedArgs = arguments.map { it.execute(varScope) }
        // todo check repeating param names. (in parser!)
        val newVarScope = funDef.params.zip(computedArgs).toMap()
        
        
        try {
            return funDef.expr.execute(newVarScope)
        } catch (e: java.lang.StackOverflowError) {
            throw StackOverflowError(this)
        }
    }
    
    
    fun BinOperator.execute(varScope: VarScope = emptyMap()): Int {
        val leftValue = left.execute(varScope)
        val rightValue = right.execute(varScope)
        
        return when (operator) {
            "+" -> leftValue + rightValue
            "-" -> leftValue - rightValue
            "*" -> leftValue * rightValue
            "/" -> if (rightValue == 0) throw RuntimeError(this) else leftValue / rightValue
            "%" -> leftValue % rightValue
            "<" -> if (leftValue < rightValue) 1 else 0
            ">" -> if (leftValue > rightValue) 1 else 0
            "=" -> if (leftValue == rightValue) 1 else 0
            else -> throw IllegalStateException("Unreachable: unknown operation in BinOperator.execute().")
        }
    }
    
    
    fun execute() = program.expr.execute()
}