package com.pavelperc.interpreter.ast

// AST nodes:
sealed class Expr {
    abstract val lineNumber : Int
}

data class NumberExpr(val value: Int, override val lineNumber: Int) : Expr() {
    override fun toString() = value.toString()
}

data class Identifier(val name: String, override val lineNumber: Int) : Expr() {
    override fun toString() = name
}

data class BinOperator(val left: Expr, val operator: String, val right: Expr) : Expr() {
    override fun toString() = "($left$operator$right)"
    override val lineNumber: Int
        get() = left.lineNumber
}

data class CallExpr(val funName: String, val arguments: List<Expr>, override val lineNumber: Int) : Expr() {
    override fun toString() = "$funName(${arguments.joinToString(",")})"
}

data class IfExpr(val condition: Expr, val onTrue: Expr, val onFalse: Expr) : Expr() {
    override fun toString() = "[$condition]?{$onTrue}:{$onFalse}"
    override val lineNumber: Int
        get() = condition.lineNumber
}

data class FunDef(val funName: String, val params: List<String>, val expr: Expr) {
    override fun toString() = "FunDef(\n\tfunName='$funName',\n\tparams=$params,\n\texpr=$expr)"
    val lineNumber: Int
        get() = expr.lineNumber
}

data class Program(val funDefs: List<FunDef>, val expr: Expr) {
    override fun toString() = "Program(\nfunDefs=${funDefs.indentToString()},\nexpr=$expr)"
}

fun <T> List<T>.indentToString(indent: Int = 1) = joinToString("\n" + "\t".repeat(indent), "[", "]")
