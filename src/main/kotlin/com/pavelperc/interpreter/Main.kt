package com.pavelperc.interpreter

import com.pavelperc.interpreter.interpreter_impl.Interpreter
import com.pavelperc.interpreter.interpreter_impl.InterpreterError
import com.pavelperc.interpreter.lexer.tokenize
import com.pavelperc.interpreter.parser.Parser
import com.pavelperc.interpreter.parser.ParserError


fun consoleLaunch(text: String, DEBUG: Boolean = true) {
    try {
        val tokens = tokenize(text)
        val parser = Parser(tokens)
        val program = parser.parseProgramOrFail()
        
        val interpreter = Interpreter(program)
        println(interpreter.execute())
        
        
    } catch (e: ParserError) {
        if (DEBUG) {
            e.printStackTrace()
        } else {
            println("SYNTAX ERROR")
        }
    } catch (e: InterpreterError) {
        if (DEBUG) {
            e.printStackTrace()
        } else {
            println(e.message)
        }
    }
}

fun main(args: Array<String>) {
//    val text = """
//        g(x)={(f(x)+f((x/2)))}
//        f(x)={[(x>1)]?{(f((x-1))+f((x-2)))}:{x}}
//        g(10)
//    """.trimIndent()
//    
//    println(text)
//    println()
//    consoleLaunch(text)
    
    outer@ while (true) {
        val lines = mutableListOf<String>()
        
        inner@ while (true) {
            val line = readLine()!!
            when (line) {
                "" -> break@inner
                "exit" -> break@outer
            }
            lines += line
        }
        val text = lines.joinToString("\n")
        
        consoleLaunch(text, DEBUG = false)
    }
}