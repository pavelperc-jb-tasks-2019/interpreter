package com.pavelperc.interpreter.parser

import com.pavelperc.interpreter.ast.*
import com.pavelperc.interpreter.ast.NumberExpr
import com.pavelperc.interpreter.lexer.Token
import com.pavelperc.interpreter.lexer.TokenType
import com.pavelperc.interpreter.lexer.TokenType.*
import com.pavelperc.interpreter.lexer.tokenize
import kotlin.math.min

// old implementation without exceptions
// may be more efficient, but is more complicated to implement

sealed class Result<T>
data class Success<T>(val value: T) : Result<T>()
data class Failure<T>(val message: String) : Result<T>() {
    fun <T> ofType() = Failure<T>(message)
}


open class ParserError(msg: String = "SYNTAX ERROR") : IllegalStateException(msg)
//class EOFError(msg: String = "SYNTAX ERROR") : ParserError(msg)

fun parser(input: String) = Parser(tokenize(input))

// all functions are not private just because of tests
class Parser(val tokens: List<Token>) {
    //    companion object {
//        const val FIRST_LINE_NUMBER = 1
//    }
    var position: Int = 0
    var lineNumber: Int = 1
    
    /** Returns current Token and moves [position] right. */
    fun nextToken(): Token? = currToken?.also { move() }
    
    /** Returns current Token, but not changes [position]. */
    val currToken: Token?
        get() = tokens.getOrNull(position)
    
    /** Increase the position. */
    fun move(step: Int = 1) {
        position += step
    }
    
    /** Restore the parser position after returning a [Failure]. */
    fun <T> withRollback(errorPrefixMsg: String = "", operation: () -> Result<T>): Result<T> {
        val savedPos = position
        val savedLine = lineNumber
        
        val result = operation()
        
        return when (result) {
            is Failure -> {
                position = savedPos
                lineNumber = savedLine
                Failure(errorPrefixMsg + result.message)
            }
            is Success -> result
        }
    }
    
    fun parseToken(type: TokenType): Result<Token> {
        val token = nextToken() ?: return Failure("No More Tokens.")
        if (token.type == type) {
            // if we parsed eol - increase the line number.
            if (token.type == EOL) {
                lineNumber++
            }
            return Success(token)
        } else {
            move(-1) // rollback
            return Failure(
                "Bad token: got ${token.type} instead of ${type}. " +
                        "Position: ${tokens.subList(
                            position,
                            min(tokens.size, position + 20)
                        ).joinToString("") { it.value }}..."
            )
        }
    }
    
    fun <A, B> Result<A>.map(transform: (A) -> B) = when (this) {
        is Success -> Success(transform(value))
        is Failure -> this.ofType<B>()
    }
    
    fun <T> anyOf(errorPrefixMsg: String = "", vararg operations: () -> Result<out T>): Result<T> {
        require(operations.isNotEmpty()) { "No Alternatives in anyOf. " }
        
        for (i in 0 until operations.size - 1) {
            val result = withRollback {
                operations[i]().map { it as T }
            }
            if (result is Success)
                return result
        }
        
        return withRollback(errorPrefixMsg) {
            operations.last()().map { it as T }
        }
    }
    
    
    fun <T> optional(operation: () -> Result<T>): T? {
        val result = withRollback { operation() }
        return when (result) {
            is Success -> result.value
            is Failure -> null
        }
    }
    
    // I expanded the given bnf grammar with repetition. 
    /** Calls [operation] while it is success. The result is always success. */
    fun <T : Any> repeated(operation: () -> Result<T>): List<T> =
        generateSequence { withRollback { operation() } }
            .takeWhile { it is Success }
            .map { (it as Success).value }
            .toList()
    
    
    fun parseConstantExpr(): Result<NumberExpr> {
        return anyOf<NumberExpr>("parseConstantExpr: ", {
            // "-" <number>
            parseToken(MINUS).unwrap { return@anyOf it.ofType() /* return failure of type NumberExpr. */ }
            val num = parseToken(NUMBER).unwrap { return@anyOf it.ofType() }
            Success(NumberExpr(-num.value.toInt(), lineNumber))
        }, {
            // <number>
            val num = parseToken(NUMBER).unwrap { return@anyOf it.ofType() }
            Success(NumberExpr(num.value.toInt(), lineNumber))
        })
    }
    
    fun parseExpr(): Result<Expr> {
        return anyOf<Expr>("parseExpr:", {
            parseConstantExpr()
        }, {
            parseBinaryExpr()
        }, {
            parseIfExpr()
        }, {
            parseCallExpr()
        }, {
            // there was an error in grammar:
            // <identifier> should go after <call-expression>
            val id = parseToken(ID).unwrap { return@anyOf it.ofType<Expr>() }
            Success(Identifier(id.value, lineNumber))
        })
    }
    
    fun parseOperator(): Result<Token> {
        val operators = setOf(MINUS, PLUS, DIV, MULT, MOD, LESS, GREATER, EQ)
        return withRollback<Token>("parseOperator: ") {
            val token = nextToken() ?: return@withRollback Failure("No More Tokens.")
            
            if (token.type in operators) {
                Success(token)
            } else {
                Failure("Not found operator.")
            }
        }
    }
    
    /** Calls [onFailure] if it is a [Failure] or returns an unwrapped [Success] value.
     * As it is inline, [onFailure] should return [Nothing] (throw an exception or outer return) */
    inline fun <T> Result<T>.unwrap(onFailure: (Failure<T>) -> Nothing) =
        when (this) {
            is Failure -> onFailure(this)
            is Success -> value
        }
    
    fun parseBinaryExpr(): Result<BinOperator> {
        parseToken(LPAR).unwrap { failure -> return failure.ofType<BinOperator>() }
        
        val left = parseExpr().unwrap { return it.ofType() }
        val op = parseOperator().unwrap { return it.ofType() }
        val right = parseExpr().unwrap { return it.ofType() }
        parseToken(RPAR).unwrap { return it.ofType() }
        
        return Success(BinOperator(left, op.value, right))
    }
    
    fun parseArgumentList(): Result<List<Expr>> {
        return withRollback<List<Expr>>("parseArgumentList: ") {
            val first = parseExpr().unwrap { return@withRollback it.ofType() }
            
            val other = repeated<Expr> {
                parseToken(COMMA).unwrap { return@repeated it.ofType() }
                parseExpr()
            }
            Success(listOf(first) + other)
        }
    }
    
    fun parseCallExpr(): Result<CallExpr> {
        return withRollback<CallExpr>("parseCallExpr: ") {
            val name = parseToken(ID).unwrap { return@withRollback it.ofType() }
            
            parseToken(LPAR).unwrap { return@withRollback it.ofType() }
            // updated grammar: args are optional now!
            val args = optional { parseArgumentList() } ?: emptyList()
            parseToken(RPAR).unwrap { return@withRollback it.ofType() }
            
            Success(CallExpr(name.value, args, lineNumber))
        }
    }
    
    fun parseIfExpr(): Result<IfExpr> {
        return withRollback<IfExpr> {
            parseToken(L_SQ_BR).unwrap { return@withRollback it.ofType() }
            val condition = parseExpr().unwrap { return@withRollback it.ofType() }
            parseToken(R_SQ_BR).unwrap { return@withRollback it.ofType() }
            parseToken(QUESTION_MARK).unwrap { return@withRollback it.ofType() }
            
            // there was an error in the given grammar: LBRACE, not LPAR
            parseToken(LBRACE).unwrap { return@withRollback it.ofType() }
            val first = parseExpr().unwrap { return@withRollback it.ofType() }
            parseToken(RBRACE).unwrap { return@withRollback it.ofType() }
            
            parseToken(COLON).unwrap { return@withRollback it.ofType() }
            
            parseToken(LBRACE).unwrap { return@withRollback it.ofType() }
            val second = parseExpr().unwrap { return@withRollback it.ofType() }
            parseToken(RBRACE).unwrap { return@withRollback it.ofType() }
            
            Success(IfExpr(condition, first, second))
        }
    }
    
    fun parseParamList(): Result<List<String>> {
        return withRollback<List<String>>("parseParamList: ") {
            val first = parseToken(ID).unwrap { return@withRollback it.ofType() }.value
            
            val other = repeated<String> {
                parseToken(COMMA).unwrap { return@repeated it.ofType() }
                parseToken(ID).map { it.value }
            }
            Success(listOf(first) + other)
        }
    }
    
    
    fun parseFunDef(): Result<FunDef> {
        return withRollback<FunDef>("parseFunDef: ") {
            val name = parseToken(ID).unwrap { return@withRollback it.ofType() }.value
            parseToken(LPAR).unwrap { return@withRollback it.ofType() }
            
            // updated grammar: params are optional now!
            val params = optional { parseParamList() } ?: emptyList()
            parseToken(RPAR).unwrap { return@withRollback it.ofType() }
            parseToken(EQ).unwrap { return@withRollback it.ofType() }
            parseToken(LBRACE).unwrap { return@withRollback it.ofType() }
            val expr = parseExpr().unwrap { return@withRollback it.ofType() }
            parseToken(RBRACE).unwrap { return@withRollback it.ofType() }
            
            Success(FunDef(name, params, expr))
        }
    }
    
    // there was an error in the given grammar rule: no EOL after the list 
    fun parseFunDefList(): List<FunDef> {
        return repeated<FunDef> {
            val funDef = parseFunDef().unwrap { return@repeated it.ofType() }
            parseToken(EOL).unwrap { return@repeated it.ofType() }
            Success(funDef)
        }
    }
    
    fun parseProgram(): Result<Program> {
        return withRollback<Program> {
            val funDefs = parseFunDefList()
            
            val expr = parseExpr().unwrap { return@withRollback it.ofType() }
            parseToken(EOF).unwrap { return@withRollback it.ofType() }
            
            Success(Program(funDefs, expr))
        }
    }
    
    fun parseProgramOrFail(): Program {
        val result = parseProgram()
        return when(result) {
            is Success -> result.value
            is Failure -> throw ParserError(result.message)
        }
    }
    
    
}