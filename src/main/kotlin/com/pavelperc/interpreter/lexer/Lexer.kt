package com.pavelperc.interpreter.lexer

import com.pavelperc.interpreter.parser.ParserError
import com.pavelperc.interpreter.lexer.TokenType.*
import kotlin.math.min

// Lexer:

typealias TokenParser = TokenType.(CharSequence) -> Token?

/** Creates a [Token] of this [TokenType] if the [input] sequence starts from symbols in [range]. */
fun charListParser(range: List<Char>): TokenParser = { input ->
    input.takeWhile { it in range }
        .let {
            if (it.isEmpty()) {
                null
            } else {
                Token(this, it.toString())
            }
        }
}

fun charListParser(range: CharRange) = charListParser(range.toList())

enum class TokenType(
    private val pattern: String,
    private val customMatcher: TokenParser? = null
) {
    // made them as concrete as possible, to eliminate collisions.
    ID("", charListParser(('A'..'Z') + ('a'..'z') + '_')),
    NUMBER("", charListParser('0'..'9')),
    L_SQ_BR("["),
    R_SQ_BR("]"),
    QUESTION_MARK("?"),
    COLON(":"),
    COMMA(","),
    LPAR("("),
    RPAR(")"),
    LBRACE("{"),
    RBRACE("}"),
    EOL("\n"),
    EQ("="),
    PLUS("+"),
    MINUS("-"),
    MULT("*"),
    DIV("/"),
    MOD("%"),
    LESS("<"),
    GREATER(">"),
    EOF("", { null }); // shouldn't be matched.

//    private val regex: Regex? = if (isRegex) Regex("($pattern)[.\n\r]*", RegexOption.MULTILINE) else null
    
    /** Creates a token with this type or returns a null. It searches the token in the beginning of [input]. */
    fun match(input: CharSequence): Token? =
        if (customMatcher != null) {
            customMatcher.invoke(this, input)
        } else {
            if (input.startsWith(pattern)) Token(this, pattern) else null
        }
}

data class Token(val type: TokenType, val value: String)

class LexerError(msg: String = "Lexer error.") : ParserError(msg)

fun tokenize(input: String): List<Token> {
    val tokens = mutableListOf<Token>()
    
    val tokenTypes = TokenType.values()
    
    var position = 0
    while (position < input.length) {
        
        val token = tokenTypes
            .asSequence()
            .map { tokenType ->
                tokenType.match(input.subSequence(position, input.length))
            }
            .firstOrNull { it != null }
            ?: throw LexerError("Unknown token: ${input.substring(position, min(position + 20, input.length))}...")
        
        position += token.value.length
        tokens += token
    }
    tokens += Token(EOF, "")
    
    return tokens
}