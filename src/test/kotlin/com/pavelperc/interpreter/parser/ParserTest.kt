package com.pavelperc.interpreter.parser

import com.pavelperc.interpreter.ast.*
import com.pavelperc.interpreter.lexer.Token
import com.pavelperc.interpreter.lexer.TokenType.*
import com.pavelperc.interpreter.lexer.tokenize
import org.amshove.kluent.*
import org.junit.Test
import kotlin.test.assertFailsWith

class ParserTest {
    
    
    @Test
    fun simpleTest() {
        val tokens = tokenize("hello=world")
        tokens.map { it.type } shouldEqual listOf(ID, EQ, ID, EOF)
        
        Parser(tokens).apply {
            position shouldEqual 0
            lineNumber shouldEqual 1
            
            currToken?.value shouldEqual "hello"
        }
    }
    
    @Test
    fun testMoving() {
        val tokens = tokenize("hello=world")
        tokens.map { it.type } shouldEqual listOf(ID, EQ, ID, EOF)
        
        Parser(tokens).apply {
            position shouldEqual 0
            currToken?.value shouldEqual "hello"
            position shouldEqual 0
            nextToken()?.value shouldEqual "hello"
            position shouldEqual 1
            nextToken()?.value shouldEqual "="
            position shouldEqual 2
            move()
            position shouldEqual 3
            
            move(-3)
            currToken?.value shouldEqual "hello"
            move(3)
            currToken?.type shouldEqual EOF
            nextToken()?.type shouldEqual EOF
            position shouldEqual 4
            
            currToken.shouldBeNull()
            nextToken().shouldBeNull()
            position shouldEqual 4
        }
    }
    
    @Test
    fun withRollBackTest() {
        val tokens = tokenize("hello=world")
        tokens.map { it.type } shouldEqual listOf(ID, EQ, ID, EOF)
        
        Parser(tokens).apply {
            position shouldEqual 0
            lineNumber shouldEqual 1
            
            withRollback {
                move()
                move()
                lineNumber += 1
                move()
                position shouldEqual 3
                Success(Unit)
            }
            position shouldEqual 3
            lineNumber shouldEqual 2
            
            position = 0
            lineNumber = 1
            withRollback("add: ") {
                move(4)
                lineNumber += 5
                position shouldEqual 4
                // exception will be rethrown
                Failure<Unit>("msg")
            } shouldEqual Failure("add: msg")
            
            position shouldEqual 0
            lineNumber shouldEqual 1
        }
    }
    
    // unwrap success or failure
    fun <T> Result<T>.shouldSuccess() = this.shouldBeInstanceOf<Success<T>>().value
    
    fun <T> Result<T>.shouldFail() = this.shouldBeInstanceOf<Failure<T>>().message
    
    @Test
    fun testParseToken() {
        val tokens = tokenize("hello=world")
        tokens.map { it.type } shouldEqual listOf(ID, EQ, ID, EOF)
        
        Parser(tokens).apply {
            position shouldEqual 0
            val token = parseToken(ID).shouldSuccess()
            token.type shouldEqual ID
            token.value shouldEqual "hello"
            position shouldEqual 1
            
            parseToken(MINUS).shouldFail()
            position shouldEqual 1
        }
    }
    
    @Test
    fun testAnyOf() {
        val tokens = tokenize("hello=world")
        tokens.map { it.type } shouldEqual listOf(ID, EQ, ID, EOF)
        
        Parser(tokens).apply {
            val ans = anyOf("", {
                position shouldEqual 0
                move(5)
                // fails with EOFError()
                parseToken(ID)
            }, {
                position shouldEqual 0
                parseToken(ID)
            })
            
            ans.shouldSuccess().value shouldEqual "hello"
            position shouldEqual 1
            
            // and failure case
            
            val ans2 = anyOf<Unit>("", {
                move(5)
                Failure("err1")
            }, {
                move(3)
                Failure("err2")
            })
            ans2.shouldFail() shouldEqual "err2"
            position shouldEqual 1
        }
    }
    
    @Test
    fun testRepeated() {
        val tokens = tokenize("hello=world")
        tokens.map { it.type } shouldEqual listOf(ID, EQ, ID, EOF)
        
        Parser(tokens).apply {
            
            repeated { parseToken(EOL) }.shouldBeEmpty()
            
            val names = repeated {
                anyOf("", { parseToken(ID) }, { parseToken(EQ) })
            }
            names.map { it.value } shouldEqual listOf("hello", "=", "world")
            currToken?.type shouldEqual EOF
        }
    }
    
    @Test
    fun testOptional() {
        val tokens = tokenize("hello=world")
        tokens.map { it.type } shouldEqual listOf(ID, EQ, ID, EOF)
        
        Parser(tokens).apply {
            position shouldEqual 0
            optional<Token> {
                parseToken(ID).unwrap { return@optional it.ofType() }
                position shouldEqual 1
                parseToken(NUMBER)
            }.shouldBeNull()
            position shouldEqual 0
            
            optional { parseToken(ID).map { it.value } }
                .shouldNotBeNull()
                .shouldEqual("hello")
            position shouldEqual 1
        }
    }
    
    @Test
    fun testExpr() {
        parser("a").parseExpr().shouldSuccess() shouldEqual Identifier("a", 1)
        
        parser("(a+2)").parseExpr().shouldSuccess() shouldEqual BinOperator(
            Identifier("a", 1), "+", NumberExpr(2, 1)
        )
        
        
        parser("1+2+3").apply {
            parseExpr().shouldSuccess() // will parse 1
            parseToken(EOF).shouldFail()
        }
        
        
        parser("(a()-b(c,1))").parseExpr().shouldSuccess() shouldEqual BinOperator(
            CallExpr("a", emptyList(), 1),
            "-", CallExpr(
                "b", listOf(
                    Identifier("c", 1), NumberExpr(1, lineNumber = 1)
                ), 1
            )
        )
        
        parser("[(2>1)]?{a}:{b}").parseExpr().shouldSuccess() shouldEqual IfExpr(
            BinOperator(NumberExpr(2, 1), ">", NumberExpr(1, 1)),
            Identifier("a", 1),
            Identifier("b", 1)
        )
    }
    
    @Test
    fun testExprToString() {
        fun match(input: String) {
            val expr = Parser(tokenize(input)).parseExpr().shouldSuccess()
            expr.toString() shouldEqual input
        }
        
        match("(1+(2/3))")
        // a() (no args) was not supported by the given grammar, but I added.
        match("(a()%b(c,d))")
        match("[(a()*b())]?{c}:{d}")
    }
    
    @Test
    fun testFunDef() {
        parser("f(x)={(x=x)}").parseFunDef().shouldSuccess() shouldEqual FunDef(
            funName = "f",
            params = listOf("x"),
            expr = BinOperator(Identifier("x", 1), "=", Identifier("x", 1))
        )
        
        // added support for zero params
        parser("f()={1}").parseFunDef().shouldSuccess() shouldEqual FunDef(
            funName = "f",
            params = listOf(),
            expr = NumberExpr(1, 1)
        )
    }
    
    @Test
    fun testProgram() {
        val program = parser(
            """
            g(x)={1}
            f(x)={1}
            (2+2)
        """.trimIndent()
        ).parseProgram().shouldSuccess()
        
        program.funDefs.map { it.funName } shouldEqual listOf("g", "f")
        program.funDefs.map { it.lineNumber } shouldEqual listOf(1, 2)
        program.expr shouldEqual BinOperator(NumberExpr(2, 3), "+", NumberExpr(2, 3))
    }
}