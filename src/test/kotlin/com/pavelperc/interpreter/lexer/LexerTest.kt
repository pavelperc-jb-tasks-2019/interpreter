package com.pavelperc.interpreter.lexer

import org.amshove.kluent.shouldBe
import org.amshove.kluent.shouldBeNull
import org.amshove.kluent.shouldEqual
import org.amshove.kluent.shouldNotBeNull
import org.junit.Test
import com.pavelperc.interpreter.lexer.TokenType.*
import kotlin.test.assertFails
import kotlin.test.assertFailsWith

class LexerTest {
    
    @Test
    fun testCharListParser() {
        
        // this parser creates a token with the given tokenType,
        // if it matches. Or returns null otherwise.
        val parser1 = charListParser('a'..'c')
        val parser2 = charListParser(('1'..'3') + ('6'..'9'))
        
        // tokenType doesn't matters
        COMMA.parser1("abcdef")
            .shouldNotBeNull()
            .shouldEqual(Token(COMMA, "abc"))
        
        COMMA.parser2("654321")
            .shouldNotBeNull()
            .shouldEqual(Token(COMMA, "6"))
        
        COMMA.parser2("aaa")
            .shouldBeNull()
    }
    
    fun TokenType.shouldMatch(input: String, parsed: String? = null) {
        val token = this.match(input).shouldNotBeNull()
        token.type shouldBe this
        if (parsed != null) {
            token.value shouldEqual parsed
        }
    }
    
    fun TokenType.shouldNotMatch(input: String) {
        val token = this.match(input).shouldBeNull()
    }
    
    @Test
    fun testTokenTypes() {
        // test just several tokens. Others are similar.
        TokenType.COMMA.shouldMatch(", xxxx", ",")
        TokenType.COMMA.shouldNotMatch("xxx")
        TokenType.COMMA.shouldNotMatch("")
        
        
        TokenType.ID.shouldMatch("_ABC__d hello", "_ABC__d")
        TokenType.ID.shouldNotMatch("5abc")
        
        TokenType.NUMBER.shouldMatch("123", "123")
        TokenType.NUMBER.shouldNotMatch("-1")
        
        TokenType.EOL.shouldMatch("\n\n", "\n")
    }
    
    @Test
    fun testEOF() {
        // EOF should not match anything, even an empty string!!
        
        TokenType.EOF.shouldNotMatch("abc\n")
        TokenType.EOF.shouldNotMatch("\n")
        TokenType.EOF.shouldNotMatch("")
    }
    
    @Test
    fun tokenizeTest() {
        tokenize("_Abc=\n+-123?")
            .map { it.type }
            .shouldEqual(listOf(ID, EQ, EOL, PLUS, MINUS, NUMBER, QUESTION_MARK, EOF))
        
        assertFailsWith<LexerError> { tokenize("12 34") }
        assertFailsWith<LexerError> { tokenize("#@!`/'") }
        
        tokenize("z_,123").map { it.value } shouldEqual listOf("z_", ",", "123", "")
    }
}