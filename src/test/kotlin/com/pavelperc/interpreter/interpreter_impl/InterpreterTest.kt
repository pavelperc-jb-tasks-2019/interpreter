package com.pavelperc.interpreter.interpreter_impl

import com.pavelperc.interpreter.lexer.tokenize
import com.pavelperc.interpreter.parser.Parser
import com.pavelperc.interpreter.parser.ParserError
import org.amshove.kluent.shouldBeIn
import org.amshove.kluent.shouldEqual
import org.junit.AfterClass
import org.junit.BeforeClass
import org.junit.Test
import kotlin.test.assertFailsWith
import kotlin.test.fail


class InterpreterTest {
    
    companion object {
        private var time = 0L
        @BeforeClass
        @JvmStatic
        fun setUp() {
            time = System.currentTimeMillis()
        }
        @AfterClass
        @JvmStatic
        fun tearDown() {
            val delta = System.currentTimeMillis() - time
            println("Took $delta millis.")
        }
    }
    
    fun launch(text: String): Int {
        val tokens = tokenize(text)
        val parser = Parser(tokens)
        val program = parser.parseProgramOrFail()
        
        val interpreter = Interpreter(program)
        return interpreter.execute()
    }
    
    
    
    
    @Test
    fun calculator1() {
        launch("(2+2)") shouldEqual 4
    }
    
    @Test
    fun calculator2() {
        launch("(2+((3*4)/5))") shouldEqual 4
    }
    
    @Test
    fun calculator3() {
        launch("((2>1)-(5%3))") shouldEqual -1
    }
    
    @Test
    fun ifExpr() {
        launch("[((10+20)>(20+10))]?{1}:{0}") shouldEqual 0
    }
    
    @Test
    fun funSupport() {
        launch(
            """
            g(x)={(f(x)+f((x/2)))}
            f(x)={[(x>1)]?{(f((x-1))+f((x-2)))}:{x}}
            g(10)
        """.trimIndent()
        ) shouldEqual 60
    }
    
    @Test
    fun noArgFun() {
        launch(
            """
            f()={(2+2)}
            g()={(f()+f())}
            (g()-f())
        """.trimIndent()
        ) shouldEqual 4
    }
    
    
    @Test
    fun recursion() {
        withException<StackOverflowError>({
        launch(
            """
            f(x)={(g(x)+1)}
            g(x)={(f(x)-1)}
            g(10)
        """.trimIndent()
        )}) {e ->
            e.lineNumber shouldBeIn listOf(1, 2)
            if (e.lineNumber == 1) {
                e.context shouldEqual "g" // call expr
            } else {
                e.context shouldEqual "f"
            }
        }
    }
    
    
    
    
    
    @Test
    fun syntaxError() {
        // lexer error is a parser error
        assertFailsWith<ParserError> {
            launch("1 + 2 + 3 + 4 + 5")
        }
    }
    
    
    inline fun <reified T : Exception> withException(block: () -> Unit, onError: (T) -> Unit) {
        try {
            block()
            fail("No exception was thrown.")
        } catch (e: Exception) {
            (e as? T) ?: fail("Bad exception type: ${e.javaClass.simpleName}.")
            onError(e)
        }
    }
    
    @Test
    fun paramNotFound() {
        withException<ParamNotFound>({ launch("f(x)={y}\nf(10)") }) { e ->
            e.lineNumber shouldEqual 1
            e.context shouldEqual "y"
        }
    }
    
    @Test
    fun funNotFound() {
        withException<FunctionNotFound>({
            launch("g(x)={f(x)}\ng(10)")
        }) { e ->
            e.lineNumber shouldEqual 1
            e.context shouldEqual "f"
        }
    }
    
    @Test
    fun argNumberMismatch() {
        withException<ArgNumberMismatch>({ launch("g(x)={(x+1)}\ng(10,20)") }) { e ->
            e.lineNumber shouldEqual 2
            e.context shouldEqual "g"
        }
    }
    
    @Test
    fun runtimeError() {
        withException<RuntimeError>({ launch("g(a,b)={(a/b)}\ng(10,0)") }) { e ->
            e.lineNumber shouldEqual 1
            e.context shouldEqual "(a/b)"
        }
    }
}